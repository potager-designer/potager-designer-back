FROM openjdk:8 AS build_image
ENV APP_HOME=/root/dev/potager-designer-back/
RUN mkdir -p $APP_HOME/src/main/kotlin
WORKDIR $APP_HOME
COPY build.gradle gradlew gradlew.bat $APP_HOME
COPY gradle $APP_HOME/gradle
# download dependencies
RUN chmod 777 gradlew
RUN ./gradlew build -x test || return 0
COPY . .
RUN ./gradlew build -x test


FROM openjdk:8-jre
WORKDIR /root/
COPY --from=build_image /root/dev/potager-designer-back/build/libs/potager-designer-back*.jar ./potager-designer-back.jar
COPY src/main/resources/docker_config.properties /root/config.properties
EXPOSE 8080
CMD ["java","-jar","potager-designer-back.jar","--spring.config.location=file:///root/config.properties"]

