package com.aott.potagerdesigner.converter

import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter(autoApply=true)
class StringToIntArrayConverter : AttributeConverter<List<Int>, String> {

    override fun convertToDatabaseColumn(intValues: List<Int>): String {
        return intValues.joinToString(separator =",", prefix="{", postfix="}", transform={value -> value.toString()})
    }

    override fun convertToEntityAttribute(intValues: String): List<Int> {
        if(intValues == "{}"){
            return listOf<Int>()
        }
        val array = intValues.replace("{", "").replace("}", "").split(",")
        return array.map({stringVal -> stringVal.toInt()}).toList()
    }
}