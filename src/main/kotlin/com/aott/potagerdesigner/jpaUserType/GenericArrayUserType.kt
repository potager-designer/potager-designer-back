package com.aott.potagerdesigner.jpaUserType

import org.hibernate.HibernateException
import java.sql.SQLException
import org.hibernate.engine.spi.SessionImplementor
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.io.Serializable
import org.hibernate.usertype.UserType
import java.sql.Types


class GenericArrayUserType<T : Serializable> : UserType {
    private val typeParameterClass: Class<T>? = null

    @Throws(HibernateException::class)
    override fun assemble(cached: Serializable, owner: Any): Any {
        return this.deepCopy(cached)
    }

    @Throws(HibernateException::class)
    override fun deepCopy(value: Any): Any {
        return value
    }

    @Throws(HibernateException::class)
    override fun disassemble(value: Any): Serializable {
        return this.deepCopy(value) as T
    }

    @Throws(HibernateException::class)
    override fun equals(x: Any?, y: Any?): Boolean {

        return if (x == null) {
            y == null
        } else x == y
    }

    @Throws(HibernateException::class)
    override fun hashCode(x: Any): Int {
        return x.hashCode()
    }

    override fun isMutable(): Boolean {
        return true
    }

    @Throws(HibernateException::class, SQLException::class)
    override fun nullSafeGet(resultSet: ResultSet, names: Array<String>, session: SessionImplementor, owner: Any): Any? {
        if (resultSet.wasNull()) {
            return null
        }
        if (resultSet.getArray(names[0]) == null) {
            return arrayOfNulls<Int>(0)
        }

        val array = resultSet.getArray(names[0])
        return array.array as T
    }

    @Throws(HibernateException::class, SQLException::class)
    override fun nullSafeSet(statement: PreparedStatement, value: Any?, index: Int, session: SessionImplementor) {
        val connection = statement.connection
        if (value == null) {
            statement.setNull(index, SQL_TYPES[0])
        } else {
            val castObject = value as T?
            val array = connection.createArrayOf("integer", castObject as Array<Any>?)
            statement.setArray(index, array)
        }
    }

    @Throws(HibernateException::class)
    override fun replace(original: Any, target: Any, owner: Any): Any {
        return original
    }

    override fun returnedClass(): Class<T>? {
        return typeParameterClass
    }

    override fun sqlTypes(): IntArray {
        return intArrayOf(Types.ARRAY)
    }

    companion object {

        protected val SQL_TYPES = intArrayOf(Types.ARRAY)
    }
}