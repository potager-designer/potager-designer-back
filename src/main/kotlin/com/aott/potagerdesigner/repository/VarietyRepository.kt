package com.aott.potagerdesigner.repository

import com.aott.potagerdesigner.model.Variety
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface VarietyRepository : JpaRepository<Variety, Long>