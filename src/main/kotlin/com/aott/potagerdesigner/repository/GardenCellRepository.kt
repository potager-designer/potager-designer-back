package com.aott.potagerdesigner.repository

import com.aott.potagerdesigner.model.GardenCell
import com.aott.potagerdesigner.model.Variety
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.apache.el.stream.Optional
import org.springframework.data.repository.CrudRepository


@Repository
interface GardenCellRepository : JpaRepository<GardenCell, Long> {
    fun findByGardenIdAndMonthAndYearOrderByRowNbAscColumnNbAsc(gardenId: Int, month: Int, year: Int): List<GardenCell>

    fun findByGardenIdAndMonthAndYearAndRowNbAndColumnNb(gardenId: Int, month: Int, year: Int, rowNb: Int, columnNb: Int) : GardenCell?
    fun findById(id: Long): GardenCell
}

