package com.aott.potagerdesigner.model

import com.fasterxml.jackson.annotation.JsonManagedReference
import jdk.nashorn.internal.objects.annotations.Getter
import javax.persistence.*

@Entity
data class GardenCell (
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var gardenId: Int = 0,

        var rowNb: Int = 0,

        var columnNb: Int = 0,

        var month: Int = 0,

        var year: Int = 0,

        var exposition: ExpositionEnum = ExpositionEnum.FULL_SUN,

        @JsonManagedReference
        @OneToMany(fetch = FetchType.EAGER, mappedBy = "gardenCell",cascade = arrayOf(CascadeType.ALL), orphanRemoval = true)
        var plantations: MutableList<Plantation> = mutableListOf()
)
