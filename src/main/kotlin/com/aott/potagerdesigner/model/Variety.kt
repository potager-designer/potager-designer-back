package com.aott.potagerdesigner.model

import javax.persistence.*

@Entity
data class Variety (
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        val name: String = "",

        val bestExposition: ExpositionEnum = ExpositionEnum.FULL_SUN,

        val columnSpace: Long = 0,

        val rowSpace: Long = 0,

        @ElementCollection
        @Column(name="variety_id_2")
        @CollectionTable(name="neighbor_ennemies", joinColumns=arrayOf(JoinColumn(name="variety_id_1")))
        @Convert(disableConversion=true)
        val neighborEnnemies: List<Int> = listOf<Int>(),

        @ElementCollection
        @Column(name="variety_id_1")
        @CollectionTable(name="neighbor_ennemies", joinColumns=arrayOf(JoinColumn(name="variety_id_2")))
        @Convert(disableConversion=true)
        val mirrorNeighborEnnemies: List<Int> = listOf<Int>(),

        @ElementCollection
        @Column(name="variety_id_2")
        @CollectionTable(name="neighbor_friends", joinColumns=arrayOf(JoinColumn(name="variety_id_1")))
        @Convert(disableConversion=true)
        val neighborFriends: List<Int> = listOf<Int>(),

        @ElementCollection
        @Column(name="variety_id_1")
        @CollectionTable(name="neighbor_friends", joinColumns=arrayOf(JoinColumn(name="variety_id_2")))
        @Convert(disableConversion=true)
        val mirrorNeighborFriends: List<Int> = listOf<Int>(),

        val plantationMonths: List<Int> = listOf<Int>(),

        val semisMonths: List<Int> = listOf<Int>(),

        val occupationMonthNumber: Int = 0,

        var custom: Boolean = true
)