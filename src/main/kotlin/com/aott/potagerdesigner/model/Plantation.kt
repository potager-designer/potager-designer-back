package com.aott.potagerdesigner.model

import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*

@Entity
data class Plantation (
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long = 0,

        var type: PlantationTypeEnum = PlantationTypeEnum.SEMIS,

        var quantity: Int = 0,

        @JsonBackReference
        @ManyToOne
        @JoinColumn(name="garden_cell_id", nullable=false)
        var gardenCell: GardenCell = GardenCell(),

        @ManyToOne
        @JoinColumn(name="variety_id")
        var variety: Variety = Variety(),

        var occupationMonthNumber: Int = 0
) {
    fun isUpdated(oldPlantation: Plantation) : Boolean {
        return this.id == oldPlantation.id && this.variety.id == oldPlantation.variety.id
                && (this.type !== oldPlantation.type || this.quantity != oldPlantation.quantity || this.occupationMonthNumber != oldPlantation.occupationMonthNumber)

    }
}