package com.aott.potagerdesigner.model

enum class PlantationTypeEnum {
    SEMIS, PLANT
}