package com.aott.potagerdesigner.model

enum class ExpositionEnum {
    FULL_SHADE, SHADE, SUN,FULL_SUN
}