package com.aott.potagerdesigner.model

import org.hibernate.validator.constraints.NotBlank
import javax.persistence.*

@Entity
data class Garden (
        @Id @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = 0,

        @get: NotBlank
        var name: String = "",

        var nbRow: Int = 0,

        var nbColumn: Int = 0,

        var cellLength: Double = 0.0,

        var northAngleWithTopPageInDegree: Double = 0.0,
        var topWall: Double = 0.0,
        var bottomWall: Double = 0.0,
        var leftWall: Double = 0.0,
        var rightWall: Double = 0.0,

        var longitude : Double = 0.0,
        var latitude : Double = 0.0,

        @OneToMany(mappedBy = "gardenId",cascade = arrayOf(CascadeType.ALL), orphanRemoval = true)
        var gardenCells: List<GardenCell> = listOf<GardenCell>()
)
