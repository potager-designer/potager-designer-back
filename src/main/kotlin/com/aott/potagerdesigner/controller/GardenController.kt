package com.aott.potagerdesigner.controller

import com.aott.potagerdesigner.model.ExpositionEnum
import com.aott.potagerdesigner.model.Garden
import com.aott.potagerdesigner.model.Variety
import com.aott.potagerdesigner.repository.VarietyRepository
import com.aott.potagerdesigner.service.GardenCellExpositionService
import com.aott.potagerdesigner.service.GardenService
import com.aott.potagerdesigner.service.VarietyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class GardenController() {

    @Autowired
    lateinit var gardenService: GardenService
    @Autowired
    lateinit var gardenCellExpositionService: GardenCellExpositionService

    @GetMapping("/gardens/{id}/cellExposition")
    fun getGardenCellExposition(@PathVariable("id") id: Long, @RequestParam("rowNb") rowNb: Int, @RequestParam("columnNb") columnNb: Int, @RequestParam("month") month: Int, @RequestParam("year") year: Int): ExpositionEnum =
            gardenCellExpositionService.getCellExposition(id, rowNb, columnNb, month, year)

    @GetMapping("/gardens")
    fun getAllGardens(): List<Garden> =
            gardenService.getAllGardens()

    @GetMapping("/gardens/{id}")
    fun getOneGarden(@PathVariable("id") id: Long) : Garden =
            gardenService.getOneGarden(id)

    @PostMapping("/gardens")
    fun createOneGarden(@Valid @RequestBody garden: Garden ) : Garden =
            gardenService.createGarden(garden)

    @PutMapping("/gardens/{id}")
    fun updateOneGarden(@PathVariable("id") id: Long, @Valid @RequestBody garden: Garden )  =
            gardenService.updateGarden(id, garden)

    @DeleteMapping("/gardens/{id}")
    fun deleteOneGarden(@PathVariable("id") id: Long ) =
            gardenService.deleteGarden(id)

}