package com.aott.potagerdesigner.controller

import com.aott.potagerdesigner.model.ExpositionEnum
import com.aott.potagerdesigner.model.Garden
import com.aott.potagerdesigner.model.GardenCell
import com.aott.potagerdesigner.model.Variety
import com.aott.potagerdesigner.repository.VarietyRepository
import com.aott.potagerdesigner.service.GardenCellExpositionService
import com.aott.potagerdesigner.service.GardenCellService
import com.aott.potagerdesigner.service.VarietyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class GardenCellController() {
    @Autowired
    lateinit var gardenCellService: GardenCellService

    @GetMapping("/gardenCells")
    fun getGardenCells(@RequestParam("gardenId") gardenId: Int, @RequestParam("month") month: Int,@RequestParam("year") year: Int): List<GardenCell> =
            gardenCellService.getGardenCellsByGardenAndMonth(gardenId, month, year)

    @GetMapping("/gardenCells/{id}")
    fun getGardenCell(@PathVariable("id") id: Long) : GardenCell =
            gardenCellService.getOneGardenCell(id)

    @PostMapping("/gardenCells")
    fun createOneGardenCell(@Valid @RequestBody gardenCell: GardenCell) : GardenCell =
            gardenCellService.createOneGardenCell(gardenCell)

    @PutMapping("/gardenCells/{id}")
    fun updateOneGardenCell(@PathVariable("id") id: Long, @Valid @RequestBody gardenCell: GardenCell) =
            gardenCellService.updateOneGardenCell(id, gardenCell)

}