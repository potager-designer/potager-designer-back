package com.aott.potagerdesigner.controller

import com.aott.potagerdesigner.model.Garden
import com.aott.potagerdesigner.model.Variety
import com.aott.potagerdesigner.service.VarietyService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class VarietyController() {

    @Autowired
    lateinit var varietyService: VarietyService

    @GetMapping("/varieties")
    fun getAllVarieties(): List<Variety> =
            varietyService.getAllVarieties()

    @GetMapping("/varieties/{id}")
    fun getOneVariety(@PathVariable("id") id: Long) : Variety =
            varietyService.getOneVariety(id)

    @PostMapping("/varieties")
    fun createOneVariety(@Valid @RequestBody variety: Variety) : Variety =
            varietyService.createVariety(variety)

    @PutMapping("/varieties/{id}")
    fun updateOneVariety(@PathVariable("id") id: Long, @Valid @RequestBody variety: Variety) : Variety =
        varietyService.updateVariety(variety)

    @DeleteMapping("/varieties/{id}")
    fun deleteOneVariety(@PathVariable("id") id: Long) =
            varietyService.deleteVariety(id)

}