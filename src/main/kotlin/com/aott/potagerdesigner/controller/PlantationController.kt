package com.aott.potagerdesigner.controller

import com.aott.potagerdesigner.model.GardenCell
import com.aott.potagerdesigner.model.Plantation
import com.aott.potagerdesigner.service.GardenCellService
import com.aott.potagerdesigner.service.PlantationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api")
class PlantationController() {
    @Autowired
    lateinit var plantationService: PlantationService


    @PostMapping("/plantations")
    fun createOnePlantation(@Valid @RequestBody plantation: Plantation) : Plantation =
            plantationService.createOnePlantation(plantation)

    @PutMapping("/plantations/{plantationId}")
    fun updateOnePlantation(@Valid @PathVariable plantationId: Long, @Valid @RequestBody plantation: Plantation) =
            plantationService.updateOnePlantation(plantationId, plantation)

  /*  @DeleteMapping("/plantations/{plantationId}")
    fun deleteOnePlantation(@Valid @PathVariable plantationId: Long) =
            plantationService.deleteOnePlantation(plantationId)*/

}