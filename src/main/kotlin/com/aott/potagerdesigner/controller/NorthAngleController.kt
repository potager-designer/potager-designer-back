package com.aott.potagerdesigner.controller

import com.aott.potagerdesigner.model.GardenCell
import com.aott.potagerdesigner.service.GardenCellService
import com.aott.potagerdesigner.service.GardenService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * Created by AOTT on 28/01/2018.
 */
@RestController
@RequestMapping("/api")

class NorthAngleController() {
    @Autowired
    lateinit var gardenService: GardenService

    @PutMapping("/gardens/{id}/northAngle")
    fun updateNorthAngle(@PathVariable("id") gardenId: Long, @Valid @RequestBody northAngle: Double) =
            gardenService.updateNorthAngle(gardenId, northAngle)

}
