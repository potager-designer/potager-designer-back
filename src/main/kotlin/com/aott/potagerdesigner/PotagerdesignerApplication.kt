package com.aott.potagerdesigner

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext


@SpringBootApplication
class PotagerDesignerApplication

fun main(args: Array<String>) {
    SpringApplication.run(PotagerDesignerApplication::class.java, *args)
}
