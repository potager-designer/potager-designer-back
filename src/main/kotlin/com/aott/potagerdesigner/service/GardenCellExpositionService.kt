package com.aott.potagerdesigner.service

import com.aott.potagerdesigner.model.ExpositionEnum
import com.aott.potagerdesigner.model.Garden
import com.aott.potagerdesigner.util.geotoolkit.SunRelativePosition
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional
class GardenCellExpositionService {
    @Autowired
    lateinit var gardenService: GardenService

    fun getCellExposition(id: Long, rowNb: Int, columnNb: Int, month: Int, year: Int): ExpositionEnum {
        val garden: Garden = gardenService.getOneGarden(id)
        return getCellExposition(columnNb, rowNb, month, year, garden)
    }

    private fun getCellExposition(columnNb: Int, rowNb: Int, month: Int, year: Int, gardenToUpdate: Garden): ExpositionEnum {
        var sumAllExposition = 0
        val tenOClock = GregorianCalendar.getInstance()
        tenOClock.set(year, month, 15, 10, 0)
        sumAllExposition += get4PointSumExpositionAtDate(columnNb, rowNb, gardenToUpdate, tenOClock.time)
        val fourteenOClock = GregorianCalendar.getInstance()
        fourteenOClock.set(year, month, 15, 13, 30)
        sumAllExposition += get4PointSumExpositionAtDate(columnNb, rowNb, gardenToUpdate, fourteenOClock.time)
        val heighteenOClock = GregorianCalendar.getInstance()
        heighteenOClock.set(year, month, 15, 17, 0)
        sumAllExposition += get4PointSumExpositionAtDate(columnNb, rowNb, gardenToUpdate, heighteenOClock.time)
        println("sumAllExposition: "+sumAllExposition)
        if (sumAllExposition.toDouble() / 12 < 0.25) {
            println("Exposition: "+ExpositionEnum.FULL_SHADE)
            return ExpositionEnum.FULL_SHADE
        } else if (sumAllExposition.toDouble() / 12 >= 0.25 && sumAllExposition.toDouble() / 12 < 0.5) {
            println("Exposition: "+ExpositionEnum.SHADE)
            return ExpositionEnum.SHADE
        } else if (sumAllExposition.toDouble() / 12 >= 0.5 && sumAllExposition.toDouble() / 12 < 0.75) {
            println("Exposition: "+ExpositionEnum.SUN)
            return ExpositionEnum.SUN
        } else if (sumAllExposition.toDouble() / 12 >= 0.75) {
            println("Exposition: "+ExpositionEnum.FULL_SUN)
            return ExpositionEnum.FULL_SUN
        } else {
            throw Exception("Error calculating cell expo")
        }
    }

    private fun get4PointSumExpositionAtDate(columnNb: Int, rowNb: Int, garden: Garden, date: Date): Int {
        val calculator = SunRelativePosition()
        calculator.date = date
        calculator.setCoordinate(garden.longitude, garden.latitude)
        //println("Date (UTC): " + format.format(date))
        //println("Longitude:  " + longitude)
        //println("Latitude:   " + latitude)
        println("****************************")
        println("Cell ("+rowNb+","+columnNb+")")
        println("Azimuth:  " + (calculator.getAzimuth() + garden.northAngleWithTopPageInDegree))
        println("Elevation:    " + calculator.getElevation())
        println("wallShadowLength:  " + (2.0 / (Math.tan(calculator.getElevation() * Math.PI / 180))))
        //println("North:    " + garden.northAngleWithTopPageInDegree)
        //println("Noon date:  " + format.format(calculator.noonDate))
        val alpha = calculator.getAzimuth() + garden.northAngleWithTopPageInDegree
        val elevation = calculator.getElevation()

        var sumExpo = 0
        if (elevation > 0) {
            val x1 = columnNb * garden.cellLength + garden.cellLength / 4
            val y1 = garden.nbRow * garden.cellLength - (rowNb * garden.cellLength) - garden.cellLength / 4
            sumExpo += getPointExposition(x1, y1, elevation, alpha, garden)
            val x2 = columnNb * garden.cellLength + 3 * garden.cellLength / 4
            val y2 = garden.nbRow * garden.cellLength - (rowNb * garden.cellLength) - garden.cellLength / 4
            sumExpo += getPointExposition(x2, y2, elevation, alpha, garden)
            val x3 = columnNb * garden.cellLength + garden.cellLength / 4
            val y3 = garden.nbRow * garden.cellLength - (rowNb * garden.cellLength) - 3 * garden.cellLength / 4
            sumExpo += getPointExposition(x3, y3, elevation, alpha, garden)
            val x4 = columnNb * garden.cellLength + 3 * garden.cellLength / 4
            val y4 = garden.nbRow * garden.cellLength - (rowNb * garden.cellLength) - 3 * garden.cellLength / 4
            sumExpo += getPointExposition(x4, y4, elevation, alpha, garden)
        }
        println("sumExposition:"+sumExpo)
        return sumExpo
    }

    fun getPointExposition(x: Double, y: Double, elevation: Double, alpha: Double, garden: Garden): Int {
        println("Point("+x+","+y+")")
        var sum = 0
        sum += getTopWallExposition(x, y, elevation, alpha, garden)
        sum += getRightWallExposition(x, y, elevation, alpha, garden)
        sum += getLeftWallExposition(x, y, elevation, alpha, garden)
        sum += getBottomWallExposition(x, y, elevation, alpha, garden)
        return sum / 4
    }

    fun testCellExposition(garden: Garden) {
        getTopWallExposition(0.5, 4.5, 28.3558, 45.0, garden) // SHADOW
        getTopWallExposition(0.1, 5.5, 28.3558, 45.0, garden) // SUN
        getTopWallExposition(0.1, 5.5, 28.3558, 135.0, garden) // SHADOW
        getTopWallExposition(0.5, 4.5, 28.3558, 135.0, garden) // SUN
        getTopWallExposition(1.0, 5.1, 28.3558, 225.0, garden) // SHADOW
        getTopWallExposition(1.0, 4.5, 28.3558, 225.0, garden) // SUN
        getTopWallExposition(1.0, 4.7, 28.3558, 315.0, garden) // SHADOW
        getTopWallExposition(0.5, 4.4, 28.3558, 315.0, garden) // SUN
        getTopWallExposition(0.5, 3.5, 28.3558, 0.0, garden) // SHADOW
        getTopWallExposition(0.5, 1.0, 28.3558, 0.0, garden) // SUN
        getTopWallExposition(0.5, 3.5, 28.3558, 90.0, garden) // SUN
        getTopWallExposition(0.5, 2.9, 28.3558, 90.0, garden) // SUN
        getTopWallExposition(0.5, 6.9, 28.3558, 180.0, garden) // SHADOW
        getTopWallExposition(0.5, 4.9, 28.3558, 180.0, garden) // SUN
        getTopWallExposition(0.5, 3.5, 28.3558, 270.0, garden) // SUN
        getTopWallExposition(0.5, 2.9, 28.3558, 270.0, garden) // SUN
        getRightWallExposition(11.9, 3.5, 28.3558, 0.0, garden) // SUN
        getRightWallExposition(12.1, 3.5, 28.3558, 0.0, garden) // SUN
        getRightWallExposition(1.0, 3.0, 28.3558, 90.0, garden) // SUN
        getRightWallExposition(11.0, 3.0, 28.3558, 90.0, garden) // SHADOW
        getRightWallExposition(11.9, 3.5, 28.3558, 180.0, garden) // SUN
        getRightWallExposition(12.1, 3.5, 28.3558, 180.0, garden) // SUN
        getRightWallExposition(11.0, 3.0, 28.3558, 270.0, garden) // SUN
        getRightWallExposition(13.0, 3.0, 28.3558, 270.0, garden) // SHADOW
        getRightWallExposition(11.0, 0.5, 28.3558, 45.0, garden) // SHADOW
        getRightWallExposition(11.0, 4.5, 28.3558, 45.0, garden) // SUN
        getRightWallExposition(11.0, 4.5, 28.3558, 135.0, garden) // SHADOW
        getRightWallExposition(11.0, 0.0, 28.3558, 135.0, garden) // SUN
        getRightWallExposition(12.1, 0.5, 28.3558, 225.0, garden) // SHADOW
        getRightWallExposition(12.5, 0.1, 28.3558, 225.0, garden) // SUN
        getRightWallExposition(12.1, 0.1, 28.3558, 315.0, garden) // SHADOW
        getRightWallExposition(11.9, 0.1, 28.3558, 315.0, garden) // SUN
        getBottomWallExposition(1.0, -0.1, 28.3558, 45.0, garden) // SHADOW
        getBottomWallExposition(0.0, 1.0, 28.3558, 45.0, garden) // SUN
        getBottomWallExposition(0.0, 1.0, 28.3558, 135.0, garden) // SHADOW
        getBottomWallExposition(12.0, 0.5, 28.3558, 135.0, garden) // SUN
        getBottomWallExposition(0.5, 0.4, 28.3558, 225.0, garden) // SHADOW
        getBottomWallExposition(0.0, 0.5, 28.3558, 225.0, garden) // SUN
        getBottomWallExposition(0.5, -0.4, 28.3558, 315.0, garden) // SHADOW
        getBottomWallExposition(0.1, 0.1, 28.3558, 315.0, garden) // SUN
        getLeftWallExposition(-0.1, 1.0, 28.3558, 45.0, garden) // SHADOW
        getLeftWallExposition(1.0, 0.1, 28.3558, 45.0, garden) // SUN
        getLeftWallExposition(-0.1, 0.1, 28.3558, 135.0, garden) // SHADOW
        getLeftWallExposition(1.0, 5.0, 28.3558, 135.0, garden) // SUN
        getLeftWallExposition(0.1, 0.5, 28.3558, 225.0, garden) // SHADOW
        getLeftWallExposition(0.5, 0.1, 28.3558, 225.0, garden) // SUN
        getLeftWallExposition(0.1, 0.1, 28.3558, 315.0, garden) // SHADOW
        getLeftWallExposition(0.5, 4.9, 28.3558, 315.0, garden) // SUN
    }

    private fun getBottomWallExposition(cellCenterX: Double, cellCenterY: Double, elevation: Double, alpha: Double, garden: Garden): Int {
        val bottomExpo =getExposition(cellCenterX, cellCenterY, elevation, alpha, garden.bottomWall, 0.0, 0.0, garden.nbColumn * garden.cellLength, 0.0, true)
        println("Bottom Wall Expo:"+bottomExpo)
        return bottomExpo
    }

    private fun getTopWallExposition(cellCenterX: Double, cellCenterY: Double, elevation: Double, alpha: Double, garden: Garden): Int {
        val topExpo = getExposition(cellCenterX, cellCenterY, elevation, alpha, garden.topWall, 0.0, garden.nbRow * garden.cellLength, garden.nbColumn * garden.cellLength, garden.nbRow * garden.cellLength, true)
        println("Top Wall Expo:"+topExpo)
        return topExpo
    }

    private fun getRightWallExposition(cellCenterX: Double, cellCenterY: Double, elevation: Double, alpha: Double, garden: Garden): Int {
        val rightExpo = getExposition(cellCenterX, cellCenterY, elevation, alpha, garden.rightWall, garden.nbColumn * garden.cellLength, garden.nbRow * garden.cellLength, garden.nbColumn * garden.cellLength, 0.0, false)
        println("Right Wall Expo:"+rightExpo)
        return rightExpo
    }

    private fun getLeftWallExposition(cellCenterX: Double, cellCenterY: Double, elevation: Double, alpha: Double, garden: Garden): Int {
        val leftExpo = getExposition(cellCenterX, cellCenterY, elevation, alpha, garden.leftWall, 0.0, 0.0, 0.0, garden.nbRow * garden.cellLength, false)
        println("Left Wall Expo:"+leftExpo)
        return leftExpo
    }

    private fun getExposition(cellCenterX: Double, cellCenterY: Double, elevation: Double, alpha: Double, wallHeight: Double, wall0X: Double, wall0Y: Double, wallfX: Double, wallfY: Double, topOrBottomWall: Boolean): Int {
        //println("*************************************************")
        //println("elevation:"+ elevation)
        //println("alpha:"+ alpha)
        var correctedAlpha = alpha
        if (!topOrBottomWall) {
            correctedAlpha = 90 - alpha
        }
        //println("cellCenterX:"+ cellCenterX)
        //println("cellCenterY:"+ cellCenterY)
        val wallShadowLength = wallHeight / (Math.tan(elevation * Math.PI / 180))
        //println("wallShadowLength:  " + wallShadowLength)
        val wallShadowProjection = -wallShadowLength * Math.cos(correctedAlpha * Math.PI / 180)

        var checkB = true
        if (alpha % 90.0 != 0.0) {
            //println("wallShadowProjection:  " + wallShadowProjection)
            val bCell = getB(cellCenterX, cellCenterY, alpha)
            //println("bCell:  " + bCell)
            val b0TopWall = getB(wall0X, wall0Y, alpha)
            //println("b0TopWall:  " + b0TopWall)
            val bfTopWall = getB(wallfX, wallfY, alpha)
            //println("bfTopWall:  " + bfTopWall)
            val maxB = Math.max(b0TopWall, bfTopWall)
            val minB = Math.min(b0TopWall, bfTopWall)
            checkB = bCell > minB && bCell < maxB
        }
        if (topOrBottomWall) {
            val maxX = Math.max(wall0Y, wall0Y + wallShadowProjection)
            val minX = Math.min(wall0Y, wall0Y + wallShadowProjection)
            if (cellCenterY > minX && cellCenterY < maxX && checkB) {
                //println("cell expostion according to top wall:" + ExpositionEnum.SHADE)
                return 0
            } else {
                //println("cell expostion according to top wall:" + ExpositionEnum.SUN)
                return 1
            }
        } else {
            val maxX = Math.max(wall0X, wall0X + wallShadowProjection)
            val minX = Math.min(wall0X, wall0X + wallShadowProjection)
            if (cellCenterX > minX && cellCenterX < maxX && checkB) {
                //println("cell expostion according to top wall:" + ExpositionEnum.SHADE)
                return 0
            } else {
                //println("cell expostion according to top wall:" + ExpositionEnum.SUN)
                return 1
            }
        }
    }

    //ordonnée à l'origine - vertical intercept
    fun getB(x: Double, y: Double, alpha: Double): Double {
        return y - x / Math.tan(alpha * Math.PI / 180)
    }
}