package com.aott.potagerdesigner.service

import com.aott.potagerdesigner.model.GardenCell
import com.aott.potagerdesigner.model.Plantation
import com.aott.potagerdesigner.repository.GardenCellRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestParam
import javax.transaction.Transactional

@Service
@Transactional
class GardenCellService {

    @Autowired
    lateinit var gardenCellRepository: GardenCellRepository

    fun getGardenCellsByGardenAndMonth(gardenId: Int, month: Int, year: Int): List<GardenCell> =
            gardenCellRepository.findByGardenIdAndMonthAndYearOrderByRowNbAscColumnNbAsc(gardenId, month, year)

    fun getGardenCellsByGardenAndMonthAndPosition(gardenId: Int, month: Int, year: Int, row: Int, column: Int): GardenCell? =
            gardenCellRepository.findByGardenIdAndMonthAndYearAndRowNbAndColumnNb(gardenId, month, year, row, column)

    fun getOneGardenCell(id: Long) : GardenCell =
            gardenCellRepository.findOne(id)

    fun updateOneGardenCell(id: Long, newGardenCell: GardenCell) {
        val oldGardenCell = gardenCellRepository.getOne(id)
       val oldPlantations = oldGardenCell.plantations
        println("oldPlantations:"+oldPlantations.size)
         val plantationsToAdd: List<Plantation> = newGardenCell.plantations.filter({plantation -> plantation.id == 0L})
        val plantationsToDelete: List<Plantation> = oldPlantations.filter({oldPlantation -> !newGardenCell.plantations.any({newPlantation -> newPlantation.id == oldPlantation.id})})
        val plantationsToUpdate : List<Plantation> = newGardenCell.plantations.filter({newPlantation -> oldPlantations.any({oldPlantation -> newPlantation.id == oldPlantation.id && newPlantation.isUpdated(oldPlantation)})})

        println("plantationsToAdd:"+plantationsToAdd.size)
        val maxOccupationPlantationToAdd = plantationsToAdd.maxWith (Comparator({a, b -> a.occupationMonthNumber.compareTo(b.occupationMonthNumber)}))
        //println("maxOccupationPlantationToAdd:"+maxOccupationPlantationToAdd)
        println("plantationsToDelete:"+plantationsToDelete.size)
        val maxOccupationPlantationToDelete = plantationsToDelete.maxWith (Comparator({a, b -> a.occupationMonthNumber.compareTo(b.occupationMonthNumber)}))
        //println("maxOccupationPlantationToDelete:"+maxOccupationPlantationToDelete)
        println("plantationsToUpdate:"+plantationsToUpdate.size)
        val maxOccupationPlantationToUpdate = plantationsToUpdate.maxWith (Comparator({a, b -> a.occupationMonthNumber.compareTo(b.occupationMonthNumber)}))
        //println("maxOccupationPlantationToUpdate:"+maxOccupationPlantationToUpdate)
        var maxOccupationPlantationCount = 0
        if(maxOccupationPlantationToAdd != null) {
            maxOccupationPlantationCount = maxOccupationPlantationToAdd.occupationMonthNumber
        }
        if(maxOccupationPlantationToDelete != null && maxOccupationPlantationToDelete.occupationMonthNumber > maxOccupationPlantationCount){
            maxOccupationPlantationCount = maxOccupationPlantationToDelete.occupationMonthNumber
        }
        if(maxOccupationPlantationToUpdate != null && maxOccupationPlantationToUpdate.occupationMonthNumber > maxOccupationPlantationCount){
            maxOccupationPlantationCount = maxOccupationPlantationToUpdate.occupationMonthNumber
        }
        println("maxOccupationPlantationCount:"+maxOccupationPlantationCount)
        for(nextMonthCount in 1 until maxOccupationPlantationCount) {
            val nextMonthAndYear = nextMonth(newGardenCell.month, newGardenCell.year, nextMonthCount)
            println("nextMonthAndYear:" + nextMonthAndYear)
            var nextMonthGardenCell: GardenCell? = this.getGardenCellsByGardenAndMonthAndPosition(newGardenCell.gardenId, nextMonthAndYear.get("month")!!, nextMonthAndYear.get("year")!!, newGardenCell.rowNb, newGardenCell.columnNb)
            println("finded:" + (nextMonthGardenCell != null));
            if (nextMonthGardenCell == null) {
                val nextMonthGardenCellToAdd = GardenCell(gardenId = newGardenCell.gardenId, rowNb = newGardenCell.rowNb, columnNb=newGardenCell.columnNb,  month = nextMonthAndYear.get("month")!!, year = nextMonthAndYear.get("year")!!, exposition =  newGardenCell.exposition)
                println("save garden cell month:"+nextMonthGardenCellToAdd.month+" year:"+nextMonthGardenCellToAdd.year)
                nextMonthGardenCell = gardenCellRepository.save(nextMonthGardenCellToAdd)
                println("new garden cell id:"+nextMonthGardenCell.id)
            }
            if (nextMonthGardenCell != null) {
                for (plantationToAdd in plantationsToAdd) {
                    if (plantationToAdd.occupationMonthNumber > nextMonthCount) {
                        println("add plantation:" + plantationToAdd.variety.name + " to garden cell id:"+nextMonthGardenCell.id)
                        val plantationReadyToAdd = Plantation(type=plantationToAdd.type,quantity = plantationToAdd.quantity, gardenCell = GardenCell(id= nextMonthGardenCell.id), variety = plantationToAdd.variety, occupationMonthNumber = plantationToAdd.occupationMonthNumber -1 )
                        nextMonthGardenCell.plantations.add(plantationReadyToAdd)
                    }
                }

                for (plantationToDelete in plantationsToDelete) {
                    if (plantationToDelete.occupationMonthNumber > nextMonthCount) {
                        println("delete plantation:" + plantationToDelete.variety + " from garden cell id:"+nextMonthGardenCell.id)
                        val plantationReadyToRemove = nextMonthGardenCell.plantations.find({ plantation -> plantation.variety.id == plantationToDelete.variety.id })
                        nextMonthGardenCell.plantations.remove(plantationReadyToRemove);
                    }
                }

                for (plantationToUpdate in plantationsToUpdate) {
                    if (plantationToUpdate.occupationMonthNumber > nextMonthCount) {
                        println("update plantation:" + plantationToUpdate.variety.name + " in garden cell id:"+nextMonthGardenCell.id)
                        val oldPlantationsReadyToUpdate = nextMonthGardenCell.plantations.find({ plantation -> plantation.variety.id == plantationToUpdate.variety.id })
                        if(oldPlantationsReadyToUpdate!=null){
                            oldPlantationsReadyToUpdate.occupationMonthNumber = plantationToUpdate.occupationMonthNumber - nextMonthCount
                            oldPlantationsReadyToUpdate.quantity = plantationToUpdate.quantity
                            oldPlantationsReadyToUpdate.type = plantationToUpdate.type
                        }
                    }
                }
                gardenCellRepository.save(nextMonthGardenCell)
            }
        }
        gardenCellRepository.save(newGardenCell)
    }

    fun createOneGardenCell(gardenCell: GardenCell) : GardenCell {
        println("save garden cell month:"+gardenCell.month+" year:"+gardenCell.year)
        var gardenCellResult = gardenCellRepository.save(gardenCell)
        createPlantationsForNextMonth(gardenCell, gardenCell.plantations)
        return gardenCellResult
    }

    private fun createPlantationsForNextMonth(newGardenCell: GardenCell, plantationsToAdd: List<Plantation>) {
        val maxOccupationPlantationToAdd = plantationsToAdd.maxWith (Comparator({a, b -> a.occupationMonthNumber.compareTo(b.occupationMonthNumber)}))
        var maxOccupationPlantationCount = 0
        if(maxOccupationPlantationToAdd != null){
            maxOccupationPlantationCount = maxOccupationPlantationToAdd.occupationMonthNumber
        }
        println("maxOccupationPlantationCount:"+maxOccupationPlantationCount)
        for(nextMonthCount in 1 until maxOccupationPlantationCount) {
            val nextMonthAndYear = nextMonth(newGardenCell.month, newGardenCell.year, nextMonthCount)
            println("nextMonthAndYear:" + nextMonthAndYear)
            var nextMonthGardenCell: GardenCell? = this.getGardenCellsByGardenAndMonthAndPosition(newGardenCell.gardenId, nextMonthAndYear.get("month")!!, nextMonthAndYear.get("year")!!, newGardenCell.rowNb, newGardenCell.columnNb)
            println("finded:" + (nextMonthGardenCell != null))
            if (nextMonthGardenCell == null) {
                val nextMonthGardenCellToAdd = GardenCell(gardenId = newGardenCell.gardenId, rowNb = newGardenCell.rowNb, columnNb=newGardenCell.columnNb,  month = nextMonthAndYear.get("month")!!, year = nextMonthAndYear.get("year")!!, exposition =  newGardenCell.exposition)
                println("save garden cell month:"+nextMonthGardenCellToAdd.month+" year:"+nextMonthGardenCellToAdd.year)
                nextMonthGardenCell = gardenCellRepository.save(nextMonthGardenCellToAdd)
                println("new garden cell id:"+nextMonthGardenCell!!.id)
            }
                for(plantationToAdd in plantationsToAdd) {
                    if (plantationToAdd.occupationMonthNumber > nextMonthCount) {
                        println("add plantation:" + plantationToAdd.variety.name + " to garden cell id:"+nextMonthGardenCell.id)
                        val plantationReadyToAdd = Plantation(type=plantationToAdd.type,quantity = plantationToAdd.quantity, gardenCell = GardenCell(id= nextMonthGardenCell.id), variety = plantationToAdd.variety, occupationMonthNumber = plantationToAdd.occupationMonthNumber -1 )
                        nextMonthGardenCell.plantations.add(plantationReadyToAdd)
                    }
                }
            gardenCellRepository.save(nextMonthGardenCell)
        }
    }

    private fun nextMonth(currentMonth: Int, currentYear: Int, increment: Int): HashMap<String, Int>{
        val nextMonth = (currentMonth + increment) % 12
        val nextYear = currentYear + (currentMonth + increment) / 12
        return hashMapOf("month" to nextMonth, "year" to nextYear)
    }
}