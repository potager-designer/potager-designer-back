package com.aott.potagerdesigner.service

import com.aott.potagerdesigner.model.ExpositionEnum
import com.aott.potagerdesigner.model.Garden
import com.aott.potagerdesigner.model.GardenCell
import com.aott.potagerdesigner.repository.GardenRepository
import com.aott.potagerdesigner.util.geotoolkit.Calendar
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional
import com.aott.potagerdesigner.util.geotoolkit.SunRelativePosition
import java.text.DateFormat
import java.util.*
import java.util.GregorianCalendar




@Service
@Transactional
class GardenService {

    @Autowired
    lateinit var gardenRepository: GardenRepository

    fun getAllGardens(): List<Garden> =
            gardenRepository.findAll()


    fun getOneGarden(id: Long) : Garden =
            gardenRepository.getOne(id)

    fun createGarden(garden: Garden) : Garden =
            gardenRepository.save(garden)

    fun updateGarden(id: Long, garden: Garden) =
            gardenRepository.save(garden)

    fun deleteGarden(id: Long) =
            gardenRepository.delete(id)

    fun updateNorthAngle(gardenId: Long, northAngle: Double) {
        val gardenToUpdate: Garden = gardenRepository.findOne(gardenId)
        gardenToUpdate.northAngleWithTopPageInDegree = northAngle
        gardenRepository.save(gardenToUpdate)
    }
}