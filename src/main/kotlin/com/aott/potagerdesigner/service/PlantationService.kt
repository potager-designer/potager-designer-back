package com.aott.potagerdesigner.service

import com.aott.potagerdesigner.model.Plantation
import com.aott.potagerdesigner.repository.PlantationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class PlantationService {

    @Autowired
    lateinit var plantationRepository: PlantationRepository

    fun getOnePlantation(plantationId: Long) : Plantation {
        return plantationRepository.findOne(plantationId)
    }

    fun createOnePlantation(plantation: Plantation) : Plantation {
        return plantationRepository.save(plantation)
    }

    fun updateOnePlantation(plantationId: Long, plantation: Plantation) {
        var plantationToUpdate : Plantation = plantationRepository.findOne(plantationId)
        plantationToUpdate.occupationMonthNumber = plantation.occupationMonthNumber
        plantationToUpdate.quantity = plantation.quantity
        plantationToUpdate.type = plantation.type
        plantationRepository.save(plantationToUpdate)
    }
}