package com.aott.potagerdesigner.service

import com.aott.potagerdesigner.model.Variety
import com.aott.potagerdesigner.repository.VarietyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional
class VarietyService {

    @Autowired
    lateinit var varietyRepository: VarietyRepository

    fun getAllVarieties(): List<Variety> =
            varietyRepository.findAll()

    fun getOneVariety(id: Long) : Variety =
            varietyRepository.getOne(id)

    fun createVariety(variety: Variety): Variety {
        variety.custom = true
        return varietyRepository.save(variety)
    }

    fun deleteVariety(id: Long) {
        return varietyRepository.delete(id)
    }

    fun updateVariety(variety: Variety): Variety {
        return varietyRepository.save(variety)
    }


}