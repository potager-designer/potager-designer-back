package com.aott.potagerdesigner.util.geotoolkit

import java.awt.geom.Point2D
import java.util.*

/**
 * Calcule la position du soleil relativement à la position de l'observateur.
 * Cette classe reçoit en entrés les coordonnées spatio-temporelles de
 * l'observateur, soit:
 *
 * <TABLE border='0'><TR><TD valign="top">
 * &nbsp;<BR></BR>
 * <UL>
 * <LI>La longitude (en degrées) de l'observateur;</LI>
 * <LI>La latitude (en degrées) de l'observateur;</LI>
 * <LI>La date et heure en heure universelle (GMT).</LI>
</UL> *
 *
 * La position du soleil calculée en sortie comprend les valeurs suivantes:
 *
 * <UL>
 * <LI>L'azimuth du soleil (en degrés dans le sens des aiguilles d'une montre depuis le nord);</LI>
 * <LI>L'élévation du soleil (en degrés par rapport a l'horizon).</LI>
</UL> *
</TD> *
 *
 * <TD><img src="doc-files/CelestialSphere.png"></img></TD>
</TR></TABLE> *
 *
 * Les algorithmes utilisés dans cette classe sont des adaptations des algorithmes
 * en javascript écrit par le "National Oceanic and Atmospheric Administration,
 * Surface Radiation Research Branch". L'application original est le
 *
 * [Solar Position Calculator](http://www.srrb.noaa.gov/highlights/sunrise/azel.html).
 *
 *
 *
 * The approximations used in these programs are very good for years between
 * 1800 and 2100. Results should still be sufficiently accurate for the range
 * from -1000 to 3000. Outside of this range, results will be given, but the
 * potential for error is higher.
 *
 * @since 2.1
 *
 *
 * @source $URL$
 * @version $Id$
 * @author Remi Eve
 * @author Martin Desruisseaux (IRD)
 */
class SunRelativePosition {

    /**
     * Sun's [elevation angle][.getElevation] at twilight, in degrees.
     * Common values are defined for the
     * [astronomical twilight][.ASTRONOMICAL_TWILIGHT] (-18°),
     * [nautical twilight][.NAUTICAL_TWILIGHT] (-12°) and
     * [civil twilight][.CIVIL_TWILIGHT] (-6°).
     * If no twilight are defined, then this value is [NaN][Double.NaN].
     * The [elevation][.getElevation] and [azimuth][.getAzimuth] are
     * set to [NaN][Double.NaN] when the sun elevation is below the twilight
     * value (i.e. during night). The default value is [.CIVIL_TWILIGHT].
     */
    /**
     * Returns the sun's [elevation angle][.getElevation] at twilight, in degrees.
     * This is the value set during the last call to [.setTwilight].
     */
    /**
     * Set the sun's [elevation angle][.getElevation] at twilight, in degrees.
     * Common values are defined for the
     * [astronomical twilight][.ASTRONOMICAL_TWILIGHT] (-18°),
     * [nautical twilight][.NAUTICAL_TWILIGHT] (-12°) and
     * [civil twilight][.CIVIL_TWILIGHT] (-6°).
     * The [elevation][.getElevation] and [azimuth][.getAzimuth] are
     * set to [NaN][Double.NaN] when the sun elevation is below the twilight
     * value (i.e. during night). The default value is [.CIVIL_TWILIGHT].
     *
     * @param twilight The new sun elevation at twilight, or [Double.NaN]
     * if no twilight value should be taken in account.
     * @throws IllegalArgumentException if the twilight value is illegal.
     */
    // TODO: provides a better (localized) message.
    var twilight = CIVIL_TWILIGHT
        @Throws(IllegalArgumentException::class)
        set(twilight) {
            if (twilight < -90 || twilight > -90) {
                throw IllegalArgumentException(twilight.toString())
            }
            field = twilight
            this.updated = false
        }

    /**
     * Heure à laquelle le soleil est au plus haut dans la journée en millisecondes
     * écoulées depuis le 1er janvier 1970.
     */
    private var noonTime: Long = 0

    /**
     * Azimuth du soleil, en degrés dans le sens des
     * aiguilles d'une montre depuis le nord.
     */
    private var azimuth: Double = 0.toDouble()

    /**
     * Elévation du soleil, en degrés par rapport a l'horizon.
     */
    private var elevation: Double = 0.toDouble()

    /**
     * Geographic coordinate where current elevation and azimuth were computed.
     * Value are in degrees of longitude or latitude.
     */
    private var latitude: Double = 0.toDouble()
    private var longitude: Double = 0.toDouble()

    /**
     * Date and time when the current elevation and azimuth were computed.
     * Value is in milliseconds ellapsed since midnight UTC, January 1st, 1970.
     */
    private var time = System.currentTimeMillis()

    /**
     * `true` is the elevation and azimuth are computed, or `false`
     * if they need to be computed.  This flag is set to `false` when the date
     * and/or the coordinate change.
     */
    private var updated: Boolean = false

    /**
     * Constructs a sun relative position calculator.
     */
    constructor() {}

    /**
     * Constructs a sun relative position calculator with the specified value
     * for the [sun elevation at twilight][.setTwilight].
     *
     * @param twilight The new sun elevation at twilight, or [Double.NaN]
     * if no twilight value should be taken in account.
     * @throws IllegalArgumentException if the twilight value is illegal.
     */
    @Throws(IllegalArgumentException::class)
    constructor(twilight: Double) {
        this.twilight = twilight
    }

    /**
     * Calculates solar position for the current date, time and location.
     * Results are reported in azimuth and elevation in degrees.
     */
    private fun compute() {
        var latitude = this.latitude
        var longitude = this.longitude

        // NOAA convention use positive longitude west, and negative east.
        // Inverse the sign, in order to be closer to OpenGIS convention.
        longitude = -longitude

        // Compute: 1) Julian day (days ellapsed since January 1, 4723 BC at 12:00 GMT).
        //          2) Time as the centuries ellapsed since January 1, 2000 at 12:00 GMT.
        val julianDay = Calendar.julianDay(this.time)
        val time = (julianDay - 2451545) / 36525

        var solarDec = sunDeclination(time)
        val eqTime = equationOfTime(time)
        this.noonTime = Math.round(solarNoonTime(longitude, eqTime) * (60 * 1000)) + this.time / DAY_MILLIS * DAY_MILLIS

        // Formula below use longitude in degrees. Steps are:
        //   1) Extract the time part of the date, in minutes.
        //   2) Apply a correction for longitude and equation of time.
        //   3) Clamp in a 24 hours range (24 hours == 1440 minutes).
        var trueSolarTime = (julianDay + 0.5 - Math.floor(julianDay + 0.5)) * 1440
        trueSolarTime += eqTime - 4.0 * longitude // Correction in minutes.
        trueSolarTime -= 1440 * Math.floor(trueSolarTime / 1440)

        // Convert all angles to radians.  From this point until
        // the end of this method, local variables are always in
        // radians. Output variables ('azimuth' and 'elevation')
        // will still computed in degrees.
        //longitude = Math.toRadians(longitude)
        latitude = Math.toRadians(latitude)
        solarDec = Math.toRadians(solarDec)

        var csz = Math.sin(latitude) * Math.sin(solarDec) + Math.cos(latitude) *
                Math.cos(solarDec) *
                Math.cos(Math.toRadians(trueSolarTime / 4 - 180))
        if (csz > +1) csz = +1.0
        if (csz < -1) csz = -1.0

        val zenith = Math.acos(csz)
        val azDenom = Math.cos(latitude) * Math.sin(zenith)

        //////////////////////////////////////////
        ////    Compute azimuth in degrees    ////
        //////////////////////////////////////////
        if (Math.abs(azDenom) > 0.001) {
            var azRad = (Math.sin(latitude) * Math.cos(zenith) - Math.sin(solarDec)) / azDenom
            if (azRad > +1) azRad = +1.0
            if (azRad < -1) azRad = -1.0

            azimuth = 180 - Math.toDegrees(Math.acos(azRad))
            if (trueSolarTime > 720) { // 720 minutes == 12 hours
                azimuth = -azimuth
            }
        } else {
            azimuth = (if (latitude > 0) 180 else 0).toDouble()
        }
        azimuth -= 360 * Math.floor(azimuth / 360)

        ////////////////////////////////////////////
        ////    Compute elevation in degrees    ////
        ////////////////////////////////////////////
        val refractionCorrection = refractionCorrection(Math.toDegrees(zenith))
        val solarZen = Math.toDegrees(zenith) - refractionCorrection

        elevation = 90 - solarZen
        if (elevation < this.twilight) {
            // do not report azimuth & elevation after twilight
            azimuth = DARK
            elevation = DARK
        }
        updated = true
    }

    /**
     * Set the geographic coordinate where to compute the [elevation][.getElevation]
     * and [azimuth][.getAzimuth].
     *
     * @param longitude The longitude in degrees. Positive values are East; negative values are West.
     * @param latitude  The latitude in degrees. Positive values are North, negative values are South.
     */
    fun setCoordinate(longitude: Double, latitude: Double) {
        var latitudeUpdated = latitude
        if (latitude > +89.8) latitudeUpdated = +89.8
        if (latitude < -89.8) latitudeUpdated = -89.8
        if (latitude != this.latitude || longitude != this.longitude) {
            this.latitude = latitudeUpdated
            this.longitude = longitude
            this.updated = false
        }
    }

    /**
     * Returns the coordinate used for [elevation][.getElevation] and
     * [azimuth][.getAzimuth] computation. This is the coordinate
     * specified during the last call to a [ setCoordinate(...)][.setCoordinate] method.
     */
    /**
     * Set the geographic coordinate where to compute the [elevation][.getElevation]
     * and [azimuth][.getAzimuth].
     *
     * @param point The geographic coordinates in degrees of longitude and latitude.
     */
    var coordinate: Point2D
        get() = Point2D.Double(longitude, latitude)
        set(point) = setCoordinate(point.x, point.y)

    /**
     * Returns the date used for [elevation][.getElevation] and
     * [azimuth][.getAzimuth] computation. This is the date specified
     * during the last call to [.setDate].
     */
    /**
     * Set the date and time when to compute the [elevation][.getElevation]
     * and [azimuth][.getAzimuth].
     *
     * @param date The date and time.
     */
    var date: Date
        get() = Date(time)
        set(date) {
            val time = date.time
            if (time != this.time) {
                this.time = time
                this.updated = false
            }
        }

    /**
     * Retourne l'azimuth en degrés.
     *
     * @return L'azimuth en degrés.
     */
    fun getAzimuth(): Double {
        if (!updated) {
            compute()
        }
        return azimuth
    }

    /**
     * Retourne l'élévation en degrés.
     *
     * @return L'élévation en degrés.
     */
    fun getElevation(): Double {
        if (!updated) {
            compute()
        }
        return elevation
    }

    /**
     * Retourne l'heure à laquelle le soleil est au plus haut. L'heure est
     * retournée en nombre de millisecondes écoulées depuis le debut de la
     * journée (minuit) en heure UTC.
     */
    fun getNoonTime(): Long {
        if (!updated) {
            compute()
        }
        return noonTime % DAY_MILLIS
    }

    /**
     * Retourne la date à laquelle le soleil est au plus haut dans la journée.
     * Cette méthode est équivalente à [.getNoonTime] mais inclue le jour
     * de la date qui avait été spécifiée à la méthode [.compute].
     */
    val noonDate: Date
        get() {
            if (!updated) {
                compute()
            }
            return Date(noonTime)
        }

    companion object {
        /**
         * Number of milliseconds in a day.
         */
        private val DAY_MILLIS = 24 * 60 * 60 * 1000

        /**
         * Valeur affectée lorsque un resultat n'est pas calculable du
         * fait de la nuit. Cette valeur concerne les valeurs de sorties
         * [.elevation] et [.azimuth].
         */
        private val DARK = java.lang.Double.NaN

        /**
         * [Elevation angle][.getElevation] of astronomical twilight, in degrees.
         * Astronomical twilight is the time of morning or evening when the sun is 18° below
         * the horizon (solar elevation angle of -18°).
         */
        val ASTRONOMICAL_TWILIGHT = -18.0

        /**
         * [Elevation angle][.getElevation] of nautical twilight, in degrees.
         * Nautical twilight is the time of morning or evening when the sun is 12° below
         * the horizon (solar elevation angle of -12°).
         */
        val NAUTICAL_TWILIGHT = -12.0

        /**
         * [Elevation angle][.getElevation] of civil twilight, in degrees. Civil
         * twilight is the time of morning or evening when the sun is 6° below the horizon
         * (solar elevation angle of -6°).
         */
        val CIVIL_TWILIGHT = -6.0

        /**
         * Calculate the equation of center for the sun. This value is a correction
         * to add to the geometric mean longitude in order to get the "true" longitude
         * of the sun.
         *
         * @param  t number of Julian centuries since J2000.
         * @return Equation of center in degrees.
         */
        private fun sunEquationOfCenter(t: Double): Double {
            val m = Math.toRadians(sunGeometricMeanAnomaly(t))
            return Math.sin(1 * m) * (1.914602 - t * (0.004817 + 0.000014 * t)) +
                    Math.sin(2 * m) * (0.019993 - t * 0.000101) +
                    Math.sin(3 * m) * 0.000289
        }

        /**
         * Calculate the Geometric Mean Longitude of the Sun.
         * This value is close to 0° at the spring equinox,
         * 90° at the summer solstice, 180° at the automne equinox
         * and 270° at the winter solstice.
         *
         * @param  t number of Julian centuries since J2000.
         * @return Geometric Mean Longitude of the Sun in degrees,
         * in the range 0° (inclusive) to 360° (exclusive).
         */
        private fun sunGeometricMeanLongitude(t: Double): Double {
            var L0 = 280.46646 + t * (36000.76983 + 0.0003032 * t)
            L0 = L0 - 360 * Math.floor(L0 / 360)
            return L0
        }

        /**
         * Calculate the true longitude of the sun. This the geometric mean
         * longitude plus a correction factor ("equation of center" for the
         * sun).
         *
         * @param  t number of Julian centuries since J2000.
         * @return Sun's true longitude in degrees.
         */
        private fun sunTrueLongitude(t: Double): Double {
            return sunGeometricMeanLongitude(t) + sunEquationOfCenter(t)
        }

        /**
         * Calculate the apparent longitude of the sun.
         *
         * @param  t number of Julian centuries since J2000.
         * @return Sun's apparent longitude in degrees.
         */
        private fun sunApparentLongitude(t: Double): Double {
            val omega = Math.toRadians(125.04 - 1934.136 * t)
            return sunTrueLongitude(t) - 0.00569 - 0.00478 * Math.sin(omega)
        }

        /**
         * Calculate the Geometric Mean Anomaly of the Sun.
         *
         * @param  t number of Julian centuries since J2000.
         * @return Geometric Mean Anomaly of the Sun in degrees.
         */
        private fun sunGeometricMeanAnomaly(t: Double): Double {
            return 357.52911 + t * (35999.05029 - 0.0001537 * t)
        }

        /**
         * Calculate the true anamoly of the sun.
         *
         * @param  t number of Julian centuries since J2000.
         * @return Sun's true anamoly in degrees.
         */
        private fun sunTrueAnomaly(t: Double): Double {
            return sunGeometricMeanAnomaly(t) + sunEquationOfCenter(t)
        }

        /**
         * Calculate the eccentricity of earth's orbit. This is the ratio
         * `(a-b)/a` where <var>a</var> is the semi-major axis
         * length and <var>b</var> is the semi-minor axis length.   Value
         * is 0 for a circular orbit.
         *
         * @param  t number of Julian centuries since J2000.
         * @return The unitless eccentricity.
         */
        private fun eccentricityEarthOrbit(t: Double): Double {
            return 0.016708634 - t * (0.000042037 + 0.0000001267 * t)
        }

        /**
         * Calculate the distance to the sun in Astronomical Units (AU).
         *
         * @param  t number of Julian centuries since J2000.
         * @return Sun radius vector in AUs.
         */
        private fun sunRadiusVector(t: Double): Double {
            val v = Math.toRadians(sunTrueAnomaly(t))
            val e = eccentricityEarthOrbit(t)
            return 1.000001018 * (1 - e * e) / (1 + e * Math.cos(v))
        }

        /**
         * Calculate the mean obliquity of the ecliptic.
         *
         * @param  t number of Julian centuries since J2000.
         * @return Mean obliquity in degrees.
         */
        private fun meanObliquityOfEcliptic(t: Double): Double {
            val seconds = 21.448 - t * (46.8150 + t * (0.00059 - t * 0.001813))
            return 23.0 + (26.0 + seconds / 60.0) / 60.0
        }


        /**
         * Calculate the corrected obliquity of the ecliptic.
         *
         * @param  t number of Julian centuries since J2000.
         * @return Corrected obliquity in degrees.
         */
        private fun obliquityCorrected(t: Double): Double {
            val e0 = meanObliquityOfEcliptic(t)
            val omega = Math.toRadians(125.04 - 1934.136 * t)
            return e0 + 0.00256 * Math.cos(omega)
        }

        /**
         * Calculate the right ascension of the sun. Similar to the angular system
         * used to define latitude and longitude on Earth's surface, right ascension
         * is roughly analogous to longitude, and defines an angular offset from the
         * meridian of the vernal equinox.
         *
         * <P align="center"><img src="doc-files/CelestialSphere.png"></img></P>
         *
         * @param t number of Julian centuries since J2000.
         * @return Sun's right ascension in degrees.
         */
        private fun sunRightAscension(t: Double): Double {
            val e = Math.toRadians(obliquityCorrected(t))
            val b = Math.toRadians(sunApparentLongitude(t))
            val y = Math.sin(b) * Math.cos(e)
            val x = Math.cos(b)
            val alpha = Math.atan2(y, x)
            return Math.toDegrees(alpha)
        }

        /**
         * Calculate the declination of the sun. Declination is analogous to latitude
         * on Earth's surface, and measures an angular displacement north or south
         * from the projection of Earth's equator on the celestial sphere to the
         * location of a celestial body.
         *
         * @param t number of Julian centuries since J2000.
         * @return Sun's declination in degrees.
         */
        private fun sunDeclination(t: Double): Double {
            val e = Math.toRadians(obliquityCorrected(t))
            val b = Math.toRadians(sunApparentLongitude(t))
            val sint = Math.sin(e) * Math.sin(b)
            val theta = Math.asin(sint)
            return Math.toDegrees(theta)
        }

        /**
         * Calculate the Universal Coordinated Time (UTC) of solar noon for the given day
         * at the given location on earth.
         *
         * @param  lon       longitude of observer in degrees.
         * @param  eqTime    Equation of time.
         * @return Time in minutes from beginnning of day in UTC.
         */
        private fun solarNoonTime(lon: Double, eqTime: Double): Double {
            return 720.0 + lon * 4.0 - eqTime
        }

        /**
         * Calculate the difference between true solar time and mean. The "equation
         * of time" is a term accounting for changes in the time of solar noon for
         * a given location over the course of a year. Earth's elliptical orbit and
         * Kepler's law of equal areas in equal times are the culprits behind this
         * phenomenon. See the
         * <A HREF="http://www.analemma.com/Pages/framesPage.html">Analemma page</A>.
         * Below is a plot of the equation of time versus the day of the year.
         *
         * <P align="center"><img src="doc-files/EquationOfTime.png"></img></P>
         *
         * @param  t number of Julian centuries since J2000.
         * @return Equation of time in minutes of time.
         */
        private fun equationOfTime(t: Double): Double {
            val eps = Math.toRadians(obliquityCorrected(t))
            val l0 = Math.toRadians(sunGeometricMeanLongitude(t))
            val m = Math.toRadians(sunGeometricMeanAnomaly(t))
            val e = eccentricityEarthOrbit(t)
            var y = Math.tan(eps / 2)
            y *= y

            val sin2l0 = Math.sin(2 * l0)
            val cos2l0 = Math.cos(2 * l0)
            val sin4l0 = Math.sin(4 * l0)
            val sin1m = Math.sin(m)
            val sin2m = Math.sin(2 * m)

            val etime = y * sin2l0 - 2.0 * e * sin1m + 4.0 * e * y * sin1m * cos2l0
            -0.5 * y * y * sin4l0 - 1.25 * e * e * sin2m

            return Math.toDegrees(etime) * 4.0
        }

        /**
         * Computes the refraction correction angle.
         * The effects of the atmosphere vary with atmospheric pressure, humidity
         * and other variables. Therefore the calculation is approximate. Errors
         * can be expected to increase the further away you are from the equator,
         * because the sun rises and sets at a very shallow angle. Small variations
         * in the atmosphere can have a larger effect.
         *
         * @param  zenith The sun zenith angle in degrees.
         * @return The refraction correction in degrees.
         */
        private fun refractionCorrection(zenith: Double): Double {
            val exoatmElevation = 90 - zenith
            if (exoatmElevation > 85) {
                return 0.0
            }
            val refractionCorrection: Double // In minute of degrees
            val te = Math.tan(Math.toRadians(exoatmElevation))
            if (exoatmElevation > 5.0) {
                refractionCorrection = 58.1 / te - 0.07 / (te * te * te) + 0.000086 / (te * te * te * te * te)
            } else {
                if (exoatmElevation > -0.575) {
                    refractionCorrection = 1735.0 + exoatmElevation * (-518.2 + exoatmElevation * (103.4 + exoatmElevation * (-12.79 + exoatmElevation * 0.711)))
                } else {
                    refractionCorrection = -20.774 / te
                }
            }
            return refractionCorrection / 3600
        }
    }
}
