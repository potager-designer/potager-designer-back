package com.aott.potagerdesigner.util.geotoolkit

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/*
 *    Geotoolkit.org - An Open Source Java GIS Toolkit
 *    http://www.geotoolkit.org
 *
 *    (C) 2000-2012, Open Source Geospatial Foundation (OSGeo)
 *    (C) 2009-2012, Geomatys
 *
 *    This library is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation;
 *    version 2.1 of the License.
 *
 *    This library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    NOTE: permission has been given to the JScience project (http://www.jscience.org)
 *          to distribute this file under BSD-like license.
 */


/**
 * Approximations de quelques calculs astronomiques relatifs aux calendriers terrestres.
 * Les différents cycles astronomiques (notamment le jour, le mois et l'année) ne sont pas
 * constants. Par exemple, la longueur de l'année tropicale (le nombre moyen de jours entre
 * deux équinoxes vernales) était d'environ 365,242196 jours en 1900 et devrait être d'environ
 * 365,242184 jours en 2100, soit un changement d'environ 1 seconde. Cette classe permet de
 * calculer la longueur d'une année ou d'un mois à une date spécifiée. Toutefois, il est
 * important de noter que les intervalles de temps calculés par les méthodes de cette classe
 * sont des **moyennes**. Pour une année en particulier, l'intervalle de temps
 * d'un équinoxe vernale au prochain peut s'écarter de cette moyenne de plusieurs minutes.
 *
 *
 * Les calculs de la longueur de l'année tropicale sont basés sur les travaux de Laskar (1986).
 * Les calculs de la longueur des mois synodiques sont basés sur les travaux de Chapront-Touze et
 * Chapront (1988).On peut lire plus de détails au sujet des calendrier terrestre au site
 * [http://webexhibits.org/calendars/year-astronomy.html](http://webexhibits.org/calendars/year-astronomy.html)
 * ainsi que
 * [http://www.treasure-troves.com/astro/TropicalYear.html](http://www.treasure-troves.com/astro/TropicalYear.html).
 *
 * @author Martin Desruisseaux (IRD)
 * @version 3.00
 *
 * @since 2.0
 * @module
 */
object Calendar {
    /**
     * Nombre de millisecondes dans une journée. Cette constante est
     * utilisée pour convertir des intervalles de temps du Java en
     * nombre de jours.
     */
    private val MILLIS_IN_DAY = (1000 * 60 * 60 * 24).toDouble()

    /**
     * Jour julien correspondant à l'époch du Java (1er janvier 1970 à minuit).
     * Cette constante est utilisée pour convertir des dates du Java en jour
     * julien.
     *
     * La valeur [.julianDay]   du 1er janvier 2000 00:00 GMT est 2451544.5 jours.
     * La valeur [Date.getTime] du 1er janvier 2000 00:00 GMT est 10957 jours.
     */
    private val JULIAN_DAY_1970 = 2451544.5 - 10957

    /**
     * Computes the julian day.
     *
     * @param time The date in milliseconds elapsed since January 1st, 1970.
     */
    fun julianDay(time: Long): Double {
        return time / MILLIS_IN_DAY + JULIAN_DAY_1970
    }

    fun getDateFormat(): DateFormat {
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CANADA)
            format.timeZone = TimeZone.getTimeZone("UTC")
            return format
    }
}
